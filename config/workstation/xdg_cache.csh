if ( -d /xtmp  &&  -w /xtmp && "$LOGNAME" != "" ) then
  if ( -d /xtmp/$LOGNAME && -w /xtmp/$LOGNAME ) then
    setenv XDG_CACHE_HOME /xtmp/$LOGNAME
  else
    /bin/mkdir /xtmp/$LOGNAME
    /bin/chmod 0700 /xtmp/$LOGNAME
    setenv XDG_CACHE_HOME /xtmp/$LOGNAME
  endif
  setenv CCACHE_DIR $XDG_CACHE_HOME
endif
